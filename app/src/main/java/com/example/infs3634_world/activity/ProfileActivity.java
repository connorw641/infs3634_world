package com.example.infs3634_world.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.infs3634_world.R;
import com.example.infs3634_world.model.ErrorResponse;
import com.example.infs3634_world.model.NetworkResponse;
import com.example.infs3634_world.model.Score;
import com.example.infs3634_world.model.Scores;
import com.example.infs3634_world.model.User;
import com.example.infs3634_world.network.LeaderboardAPI;
import com.example.infs3634_world.network.NetworkClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ProfileActivity extends AppCompatActivity {
    final String TAG = "ProfileActivity";

    private RecyclerView.LayoutManager layoutManager;

    private TextView tv_logout, tv_profileUsername;
    private RecyclerView rv;
    private RVadapterLeaderboard adapter;

    private ArrayList<Score> scoresArray = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

//        rv = findViewById(R.id.rv_leaderboard);
//        rv.setHasFixedSize(true);
//        rv.setNestedScrollingEnabled(false);

        tv_profileUsername = findViewById(R.id.tv_profileUsername);
        tv_profileUsername.setText(User.loadUser(getApplicationContext()).getUsername());

        tv_logout = findViewById(R.id.tv_logout);
        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User.logoutUser(getApplicationContext());
                openLandingActivity();
            }
        });

//        layoutManager = new LinearLayoutManager(this);
//        rv.setLayoutManager(layoutManager);
//
//        RVadapter.RecyclerViewClickListener listener = new RVadapter.RecyclerViewClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//                Log.d(TAG, "");
//            }
//        };
//
//        networkScores();
//
//        adapter = new RVadapterLeaderboard(scoresArray, listener);
//
//        rv.setAdapter(adapter);
    }

    private void openLandingActivity() {
        Intent landingIntent = new Intent(this, LandingActivity.class);
        landingIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        startActivity(landingIntent);
    }

    private void networkScores() {
        final String funcName = "networkScores";
        Retrofit retro = NetworkClient.getRetrofitClient();
        LeaderboardAPI leaderboard = retro.create(LeaderboardAPI.class);
        Call call = leaderboard.leaderboard_my(User.loadUser(getApplicationContext()));

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                NetworkResponse nResponse = (NetworkResponse) response.body();
                Gson gson = new GsonBuilder().create();

                if(response.isSuccessful()) {
                    Log.d(TAG, "(" + funcName + ") response was successful!");

                    if(nResponse.status.equals(200)) {
                        // Successful user login
                        Scores scores = gson.fromJson(gson.toJson(nResponse.response), Scores.class);
                        scoresArray = scores.getScores();
                    } else if (nResponse.status.equals(400)) {
                        // JSON format error
                        ErrorResponse responseError = gson.fromJson(gson.toJson(nResponse.response), ErrorResponse.class);

                        Log.d(TAG, "(" + funcName + ") Status code: " + nResponse.status + ", Error: " + responseError.error);
                    } else if (nResponse.status.equals(401)) {
                        // Incorrect credentials
                        ErrorResponse responseError = gson.fromJson(gson.toJson(nResponse.response), ErrorResponse.class);

                        Log.d(TAG, "(" + funcName + ") Status code: " + nResponse.status + ", Error: " + responseError.error);
                    }
                } else {
                    Log.d(TAG, "(" + funcName + ") response was not successful!");
                }
            }
            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d(TAG, "(" + funcName + ") Error retrieving from remote resource!");
            }
        });
    }
}
