package com.example.infs3634_world.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;

import com.example.infs3634_world.model.Country;
import com.example.infs3634_world.R;
import com.example.infs3634_world.model.User;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.data.Feature;
import com.google.maps.android.data.geojson.GeoJsonFeature;
import com.google.maps.android.data.geojson.GeoJsonLayer;
import com.google.maps.android.data.geojson.GeoJsonPolygonStyle;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private static final String TAG = "MapsActivity";
    private int chapter;
    private String testTitle;

    private GoogleMap mMap;
    private GeoJsonPolygonStyle warStyle = new GeoJsonPolygonStyle();
    private GeoJsonPolygonStyle conqueredStyle = new GeoJsonPolygonStyle();
    private GeoJsonPolygonStyle lockedStyle = new GeoJsonPolygonStyle();
    private GeoJsonPolygonStyle revoltStyle = new GeoJsonPolygonStyle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);;
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Assign the googleMap object to our private variable
        mMap = googleMap;

        initMapDetails();

        chapter = User.loadUser(getApplicationContext()).getChapter();
        Log.d(TAG, "Loading chapter \"" + chapter + "\"");
        loadMapChapter(chapter);
    }

    private void initMapDetails() {
        // Load our custom map style from the raw resource folder
        MapStyleOptions mStyle = MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style);

        // Set the color styles for each of our custom country state styles
        warStyle.setFillColor(Color.RED);
        warStyle.setStrokeColor(Color.RED);

        lockedStyle.setFillColor(Color.GRAY);
        lockedStyle.setStrokeColor(Color.GRAY);

        revoltStyle.setFillColor(Color.YELLOW);
        revoltStyle.setStrokeColor(Color.YELLOW);

        conqueredStyle.setFillColor(Color.BLUE);
        conqueredStyle.setStrokeColor(Color.BLUE);

        // Assign our custom map style
        mMap.setMapStyle(mStyle);

        // Create the latitude and longitude point mid-point of our game map
        LatLng midPoint = new LatLng(4.12166667, 119.60000000);

        // Move the map camera to the mid-point
        mMap.moveCamera(CameraUpdateFactory.newLatLng(midPoint));

        // Disable the ability of the user to move the map
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
    }

    private void loadMapChapter(int chapter) {
        try {
            GeoJsonLayer highlight = new GeoJsonLayer(mMap, R.raw.countries_geo, getApplicationContext());

            Country foundCountry;

            for (GeoJsonFeature feature : highlight.getFeatures()) {
                String countryName = feature.getProperty("name");

                foundCountry = Country.searchCountry(countryName);

                if(foundCountry.isConquered(chapter)) {
                    //
                    feature.setPolygonStyle(conqueredStyle);
                } else if (foundCountry.isRevolting(chapter)) {
                    feature.setPolygonStyle(revoltStyle);
                    feature.setProperty("active","true");

                    testTitle = "Revolution in " + countryName + "!";
                } else if (foundCountry.isWar(chapter)) {
                    feature.setPolygonStyle(warStyle);
                    feature.setProperty("active","true");

                    testTitle = "War with " + countryName + "!";

//                    mMap.addMarker(new MarkerOptions()
//                            .position(new LatLng(foundCountry.getMidPointLat(), foundCountry.getMidPointLng()))
//                            .title("War with " + countryName + "!")
//                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.flag_map))
//                    );
                } else if (chapter >= 8 && feature.getProperty("name").equals("China")) {
                    feature.setProperty("ending","true");
                } else {
                    feature.setPolygonStyle(lockedStyle);
                }
            }

            highlight.setOnFeatureClickListener(new GeoJsonLayer.GeoJsonOnFeatureClickListener() {
                @Override
                public void onFeatureClick(Feature feature) {
                    if(feature.hasProperty("active")) {
                        Log.d("ON_CLICK_COUNTRY_LAYER", feature.getProperty("name"));
                        openNarrationIntent(feature.getProperty("name"));
                    } else if (feature.hasProperty("ending")) {
                        openFinalNarration();
                    } else {
                        Log.d("ON_CLICK_COUNTRY_LAYER", feature.getProperty("name"));
                        Log.d("ON_CLICK_COUNTRY_LAYER", "FEATURE NOT ACTIVE");
                    }
                }
            });

            highlight.addLayerToMap();
        } catch (Exception e) {
            Log.d(TAG, "Exception caught (loadMapChapter): " + e.toString() + "\nInputs (int chapter): " + chapter);
        }

    }

    private void openNarrationIntent(String country) {
        Intent narrationIntent = new Intent(this, NarrationActivity.class);
        narrationIntent.putExtra("chapter", chapter);
        narrationIntent.putExtra("country", country);
        if(testTitle != null) {
            narrationIntent.putExtra("mcqTitle", testTitle);
        } else {
            narrationIntent.putExtra("mcqTitle", "ERROR!");
        }


        startActivity(narrationIntent);
    }

    private void openFinalNarration() {
        Intent narrationIntent = new Intent(this, NarrationActivity.class);
        narrationIntent.putExtra("ending", true);

        startActivity(narrationIntent);
    }
}