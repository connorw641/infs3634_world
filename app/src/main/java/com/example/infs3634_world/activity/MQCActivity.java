package com.example.infs3634_world.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.infs3634_world.R;
import com.example.infs3634_world.model.Country;
import com.example.infs3634_world.model.Question;

import java.util.ArrayList;
import java.util.Random;

public class MQCActivity extends AppCompatActivity {
    private final String TAG = "MCQACtivity";
    private TextView tv_mcqTitle, tv_mcqCount, tv_mcqQuestion;
    private Button b_optionA, b_optionB, b_optionC, b_optionD;

    private ArrayList<Button> mcqButtonOptions = new ArrayList<>();
    private ArrayList<String> mcqAnswers = new ArrayList<>();

    private String country, mcqTitle, mcqQuestion;
    private int mcqQuestionNo, mcqQuestionMaxNo, mcqCorrectNo, chapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mcq);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            chapter = extras.getInt("chapter");
            country = extras.getString("country");
            mcqTitle = extras.getString("mcqTitle");
            mcqQuestionNo = extras.getInt("mcqQuestionNo");
            mcqQuestionMaxNo = extras.getInt("mcqQuestionMaxNo");
            mcqCorrectNo = extras.getInt("mcqCorrectNo");
            Log.d(TAG,"Extras found!");
        } else {
            Log.d(TAG,"EXTRAS = NULL!!!!");
        }

        Country foundCountry = Country.searchCountry(country);
        Log.d(TAG,"foundCountry = " + foundCountry.getName());
        Question currentQuestion = foundCountry.getChapterQuestion(chapter, mcqQuestionNo-1);
        Log.d(TAG,"currentQuestion: " + currentQuestion.getQuestion());

        mcqQuestion = currentQuestion.getQuestion();
        mcqAnswers.add(currentQuestion.getRightAnswer());
        for(String wrongAnswer : currentQuestion.getWrongAnswers()) {
            mcqAnswers.add(wrongAnswer);
        }

        tv_mcqTitle = findViewById(R.id.tv_mcqTitle);
        tv_mcqCount = findViewById(R.id.tv_mcqCount);
        tv_mcqQuestion = findViewById(R.id.tv_mcqQuestion);

        b_optionA = findViewById(R.id.b_optionA);
        b_optionB = findViewById(R.id.b_optionB);
        b_optionC = findViewById(R.id.b_optionC);
        b_optionD = findViewById(R.id.b_optionD);

        mcqButtonOptions.add(b_optionA);
        mcqButtonOptions.add(b_optionB);
        mcqButtonOptions.add(b_optionC);
        mcqButtonOptions.add(b_optionD);

        tv_mcqTitle.setText(mcqTitle);
        tv_mcqCount.setText("Question " + mcqQuestionNo + " of " + mcqQuestionMaxNo);
        tv_mcqQuestion.setText(mcqQuestion);

        prepMCQ();
    }

    private void prepMCQ() {
        Random rand = new Random();

        int rightAnswerIndex = rand.nextInt(mcqButtonOptions.size());
        final Button rightAnswerButton = mcqButtonOptions.get(rightAnswerIndex);
        mcqButtonOptions.remove(rightAnswerIndex);

        rightAnswerButton.setText(mcqAnswers.get(0));
        mcqAnswers.remove(0);

        rightAnswerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                correctAnswerIntent(rightAnswerButton);
            }
        });

        for(Button currentButton : mcqButtonOptions) {
            currentButton.setText(mcqAnswers.get(0));
            mcqAnswers.remove(0);
            final Button button = currentButton;

            currentButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    incorrectAnswerIntent(rightAnswerButton, button);
                }
            });
        }
    }

    private void questionAnswered() {
        mcqQuestionNo++;
    }

    private void mcqIntent() {
        if(mcqQuestionNo == mcqQuestionMaxNo+1) {
            Intent resultsIntent = new Intent(this, ResultsActivity.class);

            resultsIntent.putExtra("chapter",chapter);
            resultsIntent.putExtra("country",country);
            resultsIntent.putExtra("mcqTitle",mcqTitle);
            resultsIntent.putExtra("mcqQuestionNo",mcqQuestionNo);
            resultsIntent.putExtra("mcqQuestionMaxNo",mcqQuestionMaxNo);
            resultsIntent.putExtra("mcqCorrectNo",mcqCorrectNo);

            startActivity(resultsIntent);
        } else {
            Intent mcqIntent = new Intent(this, this.getClass());

            mcqIntent.putExtra("chapter",chapter);
            mcqIntent.putExtra("country",country);
            mcqIntent.putExtra("mcqTitle",mcqTitle);
            mcqIntent.putExtra("mcqQuestionNo",mcqQuestionNo);
            mcqIntent.putExtra("mcqQuestionMaxNo",mcqQuestionMaxNo);
            mcqIntent.putExtra("mcqCorrectNo",mcqCorrectNo);

            startActivity(mcqIntent);
        }
    }

    private void disableButtons() {
        b_optionA.setEnabled(false);
        b_optionB.setEnabled(false);
        b_optionC.setEnabled(false);
        b_optionD.setEnabled(false);
    }

    private void correctAnswerIntent(Button correct) {
        questionAnswered();
        mcqCorrectNo++;

        correct.setBackgroundColor(Color.GREEN);
        disableButtons();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                mcqIntent();
            }
        },1000);
    }

    private void incorrectAnswerIntent(Button correct, Button incorrect) {
        questionAnswered();

        correct.setBackgroundColor(Color.GREEN);
        incorrect.setBackgroundColor(Color.RED);
        disableButtons();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                mcqIntent();
            }
        },1000);
    }
}
