package com.example.infs3634_world.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.infs3634_world.R;
import com.example.infs3634_world.model.ErrorResponse;
import com.example.infs3634_world.model.LoginRequest;
import com.example.infs3634_world.model.NetworkResponse;
import com.example.infs3634_world.model.User;
import com.example.infs3634_world.network.LeaderboardAPI;
import com.example.infs3634_world.network.NetworkClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SignInActivity extends AppCompatActivity {
    private static final String TAG = "SignInActivity";

    private EditText et_username, et_password;
    private Button b_login, b_register;
    private TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        et_username = findViewById(R.id.et_username);
        et_password = findViewById(R.id.et_password);

        b_login = findViewById(R.id.b_login);
        b_register = findViewById(R.id.b_register);

        b_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    private void login() {
        String username = et_username.getText().toString();
        String password = et_password.getText().toString();

        Log.d(TAG, "Username: " + username);
        Log.d(TAG, "Password: " + password);

        networkLogin(username, password);
    }

    private void openRegisterIntent() {
        Intent registerPageIntent = new Intent(this, RegisterActivity.class);

        startActivity(registerPageIntent);
    }

    private void openLandingIntent() {
        Intent landingIntent = new Intent(this, LandingActivity.class);
        landingIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        startActivity(landingIntent);
    }

    private void networkLogin(String username, String password) {
        final String funcName = "networkLogin";
        Retrofit retro = NetworkClient.getRetrofitClient();
        LeaderboardAPI leaderboard = retro.create(LeaderboardAPI.class);
        Call call = leaderboard.login(new LoginRequest(username,password));

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
                 */
                NetworkResponse nResponse = (NetworkResponse) response.body();
                Gson gson = new GsonBuilder().create();
                String notificationText = "";

                if(response.isSuccessful()) {
                    Log.d(TAG, "(" + funcName + ") response was successful!");

                    if(nResponse.status.equals(200)) {
                        // Successful user login
                        User user = gson.fromJson(gson.toJson(nResponse.response), User.class);
                        Log.d(TAG, "Username: " + user.getUsername() + "\nToken: " + user.getToken() + "\nChapter: " + user.getChapter());

                        user.saveUser(getApplicationContext());

                        notificationText = "Welcome back, " + user.getUsername() + "!";
                        Toast.makeText(getApplicationContext(), notificationText, Toast.LENGTH_SHORT).show();

                        openLandingIntent();
                    } else if (nResponse.status.equals(400)) {
                        // JSON format error
                        ErrorResponse responseError = gson.fromJson(gson.toJson(nResponse.response), ErrorResponse.class);

                        Log.d(TAG, "(" + funcName + ") Status code: " + nResponse.status + ", Error: " + responseError.error);
                    } else if (nResponse.status.equals(401)) {
                        // Incorrect credentials
                        ErrorResponse responseError = gson.fromJson(gson.toJson(nResponse.response), ErrorResponse.class);

                        notificationText = "Incorrect Username/Password combination!";
                        Toast.makeText(getApplicationContext(), notificationText, Toast.LENGTH_SHORT).show();

                        Log.d(TAG, "(" + funcName + ") Status code: " + nResponse.status + ", Error: " + responseError.error);
                    }
                } else {
                    Log.d(TAG, "(" + funcName + ") response was not successful!");
                }
            }
            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d(TAG, "(" + funcName + ") Error retrieving from remote resource!");
            }
        });
    }
}
