package com.example.infs3634_world.activity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.infs3634_world.R;
import com.example.infs3634_world.model.Score;

import java.util.ArrayList;

public class RVadapterLeaderboard extends RecyclerView.Adapter<RVadapterLeaderboard.MyViewHolder> {
    private static final String TAG = "LeaderboardAdapter";

    //Declare whatever it is you need to populate each row of your adapter. In this example,
    // I just need the ArrayList that I made in MainActivity, plus the listener to react when I click on the RecyclerView

    ArrayList<Score> scoreArray;
    RVadapter.RecyclerViewClickListener listener;

    //Make the constructor as needed (same as with any other class)
    public RVadapterLeaderboard(ArrayList<Score> scoreArray, RVadapter.RecyclerViewClickListener listener) {
        this.scoreArray = scoreArray;
        this.listener = listener;
    }

    //unlike a regular listener, you also need the int position, hence why it's declared in the interface for RecyclerViewClick Listener
    //You can add other things that the interface needs to be able to do if needed, but don't forget to make it void methods since it is an interface
    public interface RecyclerViewClickListener {
        void onClick(View view, int position);
    }

    //When the view holder is created (see lifecycle diagram), you need to inflate the row xml file, and return the view + the listener back
    @Override
    public RVadapterLeaderboard.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_row, parent, false);
        return new RVadapterLeaderboard.MyViewHolder(v, listener);
    }

    //Whatever needs to happen when the view holder is binded to the recycler view.
    // In this case, all that needs to happen is to set the name TextView to a value from the arraylist
    @Override
    public void onBindViewHolder(RVadapterLeaderboard.MyViewHolder holder, int position) {
        holder.rvUsername.setText(scoreArray.get(position).getUsername());
        holder.rvScore.setText(scoreArray.get(position).getValue());
        holder.rvChapter.setText(scoreArray.get(position).getChapter());
    }

    //Fairly self-explanatory, replace the 0 (the default) with how big recyclerview should be
    @Override
    public int getItemCount() {
        return scoreArray.size();
    }


    //Creating the ViewHolder class to handle the row xml, notice how it implements the View.OnclickListener
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        //Declare the views from the row xml
        TextView rvUsername, rvScore, rvChapter;
        RVadapter.RecyclerViewClickListener listener;

        //Constructor
        public MyViewHolder( View itemView, RVadapter.RecyclerViewClickListener listener) {
            super(itemView);
            //set a listener to the view (i.e setting a click listener to every row of the recyclerView)
            itemView.setOnClickListener(this);

            //initialise views
            this.listener = listener;
            this.rvUsername = itemView.findViewById(R.id.rvUsername);
            this.rvScore = itemView.findViewById(R.id.rvScore);
            this.rvChapter = itemView.findViewById(R.id.rvImage);
        }

        //When testing this code, look for when this log message shows up
        @Override
        public void onClick(View v) {
            Log.d(TAG, "onClick: inside adapter on click");
            listener.onClick(v,getAdapterPosition());
        }
    }
}
