package com.example.infs3634_world.activity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.infs3634_world.R;
import com.example.infs3634_world.model.User;

public class LandingActivity extends AppCompatActivity {
    private User currentUser = null;
    private Button b_startGame;
    private ImageView iv_user_icon;
    private TextView tv_help;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        b_startGame = findViewById(R.id.b_startGame);
        b_startGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMapIntent();
            }
        });

        iv_user_icon = findViewById(R.id.iv_userIcon);
        iv_user_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openProfileIntent();
            }
        });

        tv_help = findViewById(R.id.tv_help);
        tv_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openHelpIntent();
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();

        currentUser = User.loadUser(getApplicationContext());

        if(currentUser == null) {
            openSignInIntent();
        }
    }

    private void openMapIntent() {
        Intent mapIntent = new Intent(this, MapsActivity.class);

        startActivity(mapIntent);
    }

    private void openSignInIntent() {
        Intent signInIntent = new Intent(this, SignInActivity.class);

        startActivity(signInIntent);
    }

    private void reloadLandingIntent() {
        Intent landingIntent = new Intent(this, this.getClass());
        landingIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        startActivity(landingIntent);
    }

    private void openHelpIntent() {
        Intent helpIntent = new Intent(this, HelpActivity.class);

        startActivity(helpIntent);
    }

    private void openProfileIntent() {
        Intent profileIntent = new Intent(this, ProfileActivity.class);

        startActivity(profileIntent);
    }
}
