package com.example.infs3634_world.activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.infs3634_world.R;

public class NarrationActivity extends AppCompatActivity {

    private String country, mcqTitle;
    private Integer chapter;
    private int mediaFile;
    TextView tvNarration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_narration);

        tvNarration = findViewById(R.id.tv_Narration);

        Bundle extras = getIntent().getExtras();
        if(extras.getBoolean("ending",false)) {
            MediaPlayer mPlayer = MediaPlayer.create(this, R.raw.n10);
            tvNarration.setText(getString(R.string.n1));

            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    openMapIntent();
                }
            });

            mPlayer.start();
        } else {
            chapter = extras.getInt("chapter");
            country = extras.getString("country");
            mcqTitle = extras.getString("mcqTitle");

            //tvNarration.setText();

        /*btNarration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/

            if(chapter.equals(1)) {
                mediaFile = R.raw.n1;
                tvNarration.setText(getString(R.string.n1));
            } else if(chapter.equals(2)) {
                mediaFile = R.raw.n2;
                tvNarration.setText(getString(R.string.n2));
            } else if(chapter.equals(3)) {
                mediaFile = R.raw.n3;
                tvNarration.setText(getString(R.string.n3));
            } else if(chapter.equals(4)) {
                mediaFile = R.raw.n4;
                tvNarration.setText(getString(R.string.n4));
            } else if(chapter.equals(5)) {
                mediaFile = R.raw.n5;
                tvNarration.setText(getString(R.string.n5));
            } else if(chapter.equals(6)) {
                mediaFile = R.raw.n6;
                tvNarration.setText(getString(R.string.n6));
            } else if(chapter.equals(7)) {
                mediaFile = R.raw.n7;
                tvNarration.setText(getString(R.string.n7));
            }

            MediaPlayer mPlayer = MediaPlayer.create(this, mediaFile);

            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp) {
                    openLearnIntent();
                }
            });

            mPlayer.start();
        }
    }

    private void openLearnIntent() {
        Intent learnIntent = new Intent(this, LearnActivity.class);
        learnIntent.putExtra("chapter", chapter);
        learnIntent.putExtra("country", country);
        learnIntent.putExtra("mcqTitle", mcqTitle);

        startActivity(learnIntent);
    }

    private void openMapIntent() {
        Intent mapIntent = new Intent(this, MapsActivity.class);

        startActivity(mapIntent);
    }
}
