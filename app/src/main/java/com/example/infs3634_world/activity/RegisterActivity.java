package com.example.infs3634_world.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.infs3634_world.R;
import com.example.infs3634_world.model.ErrorResponse;
import com.example.infs3634_world.model.LoginRequest;
import com.example.infs3634_world.model.NetworkResponse;
import com.example.infs3634_world.model.User;
import com.example.infs3634_world.network.LeaderboardAPI;
import com.example.infs3634_world.network.NetworkClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RegisterActivity extends AppCompatActivity {
    private final static String TAG = "RegisterActivity";

    private EditText et_username, et_password;
    private Button b_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        et_username = findViewById(R.id.et_password);
        et_password = findViewById(R.id.et_username);

        b_register = findViewById(R.id.b_register);

        b_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void createAccount() {
        String username = et_username.getText().toString();
        String password = et_password.getText().toString();

        if(networkNewAccount(username, password)) {

        } else {

        }
    }

    private boolean networkNewAccount(final String username, String password) {
        final String funcName = "networkNewAccount";

        Retrofit retro = NetworkClient.getRetrofitClient();
        LeaderboardAPI leaderboard = retro.create(LeaderboardAPI.class);
        Call call = leaderboard.login(new LoginRequest(username,password));

        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                NetworkResponse nResponse = (NetworkResponse) response.body();
                Gson gson = new GsonBuilder().create();
                String notificationText = "";

                if (response.isSuccessful()) {
                    if(nResponse.status.equals(200)) {
                        // Successful user login
                        User user = gson.fromJson(gson.toJson(nResponse.response), User.class);
                        Log.d(TAG, "Username: " + user.getUsername() + "\nToken: " + user.getToken());

                        user.saveUser(getApplicationContext());

                        notificationText = "Account \"" + user.getUsername() + "\" created!";
                        Toast.makeText(getApplicationContext(), notificationText, Toast.LENGTH_SHORT).show();

                        openLandingIntent();
                    } else if (nResponse.status.equals(400)) {
                        // JSON format error
                        ErrorResponse responseError = gson.fromJson(gson.toJson(nResponse.response), ErrorResponse.class);

                        Log.d(TAG, "(" + funcName + ") Status code: " + nResponse.status + ", Error: " + responseError.error);
                    } else if (nResponse.status.equals(401)) {
                        // Incorrect credentials
                        ErrorResponse responseError = gson.fromJson(gson.toJson(nResponse.response), ErrorResponse.class);

                        notificationText = "An account with the username \"" + username + "\" already exists!";
                        Toast.makeText(getApplicationContext(), notificationText, Toast.LENGTH_SHORT).show();

                        Log.d(TAG, "(" + funcName + ") Status code: " + nResponse.status + ", Error: " + responseError.error);
                    }
                } else {
                    Log.d(TAG, "(" + funcName + ") Error registering account, already exists");
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d(TAG, "(" + funcName + ") Error retrieving from remote resource!");
            }
        });

        return false;
    }

    private void openLandingIntent() {
        Intent landingIntent = new Intent(this, LandingActivity.class);

        startActivity(landingIntent);
    }
}
