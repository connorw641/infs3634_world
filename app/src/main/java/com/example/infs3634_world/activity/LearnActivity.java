package com.example.infs3634_world.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.infs3634_world.R;
import com.example.infs3634_world.model.Country;
import com.example.infs3634_world.model.LearnDetails;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class LearnActivity extends AppCompatActivity {
    private final String TAG = "LearnActivity";

    private Integer chapter;
    private String country, mcqTitle;

    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView rv;
    private RVadapter adapter;
    private ArrayList<LearnDetails> detailsArray = new ArrayList<>();

    private Button b_startTest;
    private TextView tv_learnName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn);

        Bundle extras = getIntent().getExtras();
        chapter = extras.getInt("chapter");
        country = extras.getString("country");
        mcqTitle = extras.getString("mcqTitle");

        rv = findViewById(R.id.rv_leaderboard);
        rv.setHasFixedSize(true);
        rv.setNestedScrollingEnabled(false);

        tv_learnName = findViewById(R.id.tv_learnName);
        tv_learnName.setText(country);
        Log.d(TAG, country);

        layoutManager = new LinearLayoutManager(this);
        rv.setLayoutManager(layoutManager);

        RVadapter.RecyclerViewClickListener listener = new RVadapter.RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                Log.d(TAG, "");
            }
        };

        if(chapter.equals(1)) {
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvHistory), getResources().getString(R.string.australiaHistory), R.drawable.au_history));
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvMilitary), getResources().getString(R.string.australiaMilitary), R.drawable.au_military));
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvPolitics), getResources().getString(R.string.australiaPolitics), R.drawable.au_politics));
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvEntertainment), getResources().getString(R.string.australiaEntertainment), R.drawable.au_entertainment));
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvCuisine), getResources().getString(R.string.australiaCuisine), R.drawable.au_cuisine));
        } else if (chapter.equals(2)) {
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvHistory), getResources().getString(R.string.indonesiaHistory), R.drawable.indo_history));
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvMilitary), getResources().getString(R.string.indonesiaMilitary), R.drawable.indo_military));
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvPolitics), getResources().getString(R.string.indonesiaPolitics), R.drawable.indo_politics));
        } else if (chapter.equals(3)) {
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvCulture), getResources().getString(R.string.indonesiaCulture), R.drawable.indo_culture));
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvEntertainment), getResources().getString(R.string.indonesiaEntertainment), R.drawable.indo_entertainment));
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvCuisine), getResources().getString(R.string.indonesiaCuisine), R.drawable.indo_cuisine));
        } else if (chapter.equals(4)) {
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvHistory), getResources().getString(R.string.koreaHistory), R.drawable.sk_history));
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvMilitary), getResources().getString(R.string.koreaMilitary), R.drawable.sk_military));
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvPolitics), getResources().getString(R.string.koreaPolitics), R.drawable.sk_politics));
        } else if (chapter.equals(5)) {
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvCulture), getResources().getString(R.string.koreaCulture), R.drawable.sk_culture));
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvEntertainment), getResources().getString(R.string.koreaEntertainment), R.drawable.sk_entertainment));
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvCuisine), getResources().getString(R.string.koreaCuisine), R.drawable.sk_cuisine));
        } else if (chapter.equals(6)) {
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvHistory), getResources().getString(R.string.chinaHistory), R.drawable.ch_history));
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvMilitary), getResources().getString(R.string.chinaMilitary), R.drawable.ch_military));
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvPolitics), getResources().getString(R.string.chinaPolitics), R.drawable.ch_politics));
        } else if (chapter.equals(7)) {
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvCulture), getResources().getString(R.string.chinaCulture), R.drawable.ch_culture));
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvHistory), getResources().getString(R.string.chinaEntertainment), R.drawable.ch_entertainment));
            detailsArray.add(new LearnDetails(getResources().getString(R.string.rvCuisine), getResources().getString(R.string.chinaCuisine), R.drawable.ch_cuisine));
        } else {
            detailsArray.add(new LearnDetails("ERROR", "ERROR", 0));
        }

        adapter = new RVadapter(detailsArray, listener);

        rv.setAdapter(adapter);

        b_startTest = findViewById(R.id.b_startTest);
        b_startTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTestIntent();
            }
        });
    }

    private void openTestIntent() {
        Intent testIntent = new Intent(this, MQCActivity.class);
        testIntent.putExtra("chapter", chapter);
        testIntent.putExtra("country", country);
        testIntent.putExtra("mcqTitle", mcqTitle);
        testIntent.putExtra("mcqQuestionNo",1);
        testIntent.putExtra("mcqQuestionMaxNo", Country.searchCountry(country).getChapterQuestionsSize(chapter));
        testIntent.putExtra("mcqCorrectNo",0);

        startActivity(testIntent);
    }
}
