package com.example.infs3634_world.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.infs3634_world.R;
import com.example.infs3634_world.model.User;

public class ResultsActivity extends AppCompatActivity {
    private double passGrade = 0.5f;
    private int chapter, mcqQuestionNo, mcqQuestionMaxNo, mcqCorrectNo;
    private String country, mcqTitle;

    private TextView tv_resultTitle, tv_count;
    private Button b_action;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        Bundle extras = getIntent().getExtras();
        chapter = extras.getInt("chapter");
        country = extras.getString("country");
        mcqTitle = extras.getString("mcqTitle");
        mcqQuestionNo = extras.getInt("mcqQuestionNo");
        mcqQuestionMaxNo = extras.getInt("mcqQuestionMaxNo");
        mcqCorrectNo = extras.getInt("mcqCorrectNo");

        tv_resultTitle = findViewById(R.id.tv_resultTitle);
        tv_count = findViewById(R.id.tv_count);

        b_action = findViewById(R.id.b_action);
        calculateResult();
    }

    private void calculateResult() {
        double result = (double) mcqCorrectNo / (double) mcqQuestionMaxNo;

        tv_count.setText(mcqCorrectNo + " / " + mcqQuestionMaxNo + String.format(" (%.2f", result) + "%)");
        tv_resultTitle.setText("Results of " + country + " test!");

        if(result > passGrade) {
            chapter++;
            b_action.setText("Continue to Chapter " + chapter);
            b_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openMapIntent();
                }
            });

            User user = User.loadUser(getApplicationContext());
            user.setChapter(chapter);
            user.saveUser(getApplicationContext());
        } else {
            b_action.setText("Re-learn");
            b_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openLearnIntent();
                }
            });
        }
    }

    private void openMapIntent() {
        Intent mapIntent = new Intent(this, MapsActivity.class);

        startActivity(mapIntent);
    }

    private void openLearnIntent() {
        Intent learnIntent = new Intent(this, LearnActivity.class);

        startActivity(learnIntent);
    }
}
