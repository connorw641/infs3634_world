package com.example.infs3634_world.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Scores {
    @SerializedName("value")
    @Expose
    private ArrayList<Score> scores;

    public ArrayList<Score> getScores() {
        return scores;
    }
}
