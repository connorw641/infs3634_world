package com.example.infs3634_world.model;

public class Question {
    private String question;
    private String[] answers;

    public Question(String question, String[] answers) {
        this.question = question;
        this.answers = answers;
    }

    public String getQuestion() {
        return question;
    }

    public String getRightAnswer() {
        return answers[0];
    }

    public String[] getWrongAnswers() {
        String[] out = {answers[1], answers[2], answers[3]};
        return out;
    }
}
