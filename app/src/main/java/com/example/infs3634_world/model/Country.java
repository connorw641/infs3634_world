package com.example.infs3634_world.model;

import java.util.ArrayList;

public class Country {
    private String name;
    private int warChapter, revoltChapter, conqueredChapter;
    private double midPointLat, midPointLng;
    private Question[] warQuestions, revoltQuestions;

    public Country(String name,double midPointLat, double midPointLng, int warChapter, int revoltChapter, int conqueredChapter, Question[] warQuestions, Question[] revoltQuestions) {
        this.name = name;
        this.midPointLat = midPointLat;
        this.midPointLng = midPointLng;
        this.warChapter = warChapter;
        this.revoltChapter = revoltChapter;
        this.conqueredChapter = conqueredChapter;
        this.warQuestions = warQuestions;
        this.revoltQuestions = revoltQuestions;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMidPointLat(double midPointLat) {
        this.midPointLat = midPointLat;
    }

    public void setMidPointLng(double midPointLng) {
        this.midPointLng = midPointLng;
    }

    public String getName() {
        return this.name;
    }

    public double getMidPointLat() {
        return this.midPointLat;
    }

    public double getMidPointLng() {
        return this.midPointLng;
    }

    public boolean isConquered(int chapter) {
        return this.conqueredChapter <= chapter;
    }

    public boolean isRevolting(int chapter) {
        return this.revoltChapter <= chapter;
    }

    public boolean isWar(int chapter) {
        return this.warChapter <= chapter;
    }

    private int getWarQuestionsSize() {
        return this.warQuestions.length;
    }

    private Question getWarQuestion(int questionNo) {
        return this.warQuestions[questionNo];
    }

    private int getRevoltQuestionsSize() {
        return this.revoltQuestions.length;
    }

    private Question getRevoltQuestion(int questionNo) {
        return this.revoltQuestions[questionNo];
    }

    public Question getChapterQuestion(int chapter, int questionNo) {
        if(this.warChapter == chapter) {
            return getWarQuestion(questionNo);
        } else {
            return getRevoltQuestion(questionNo);
        }
    }

    public int getChapterQuestionsSize(int chapter) {
        if(this.warChapter == chapter) {
            return getWarQuestionsSize();
        } else {
            return getRevoltQuestionsSize();
        }
    }

    public static ArrayList<Country> getCountries() {
        ArrayList<Country> countryArrayList = new ArrayList<Country>();

        Question[] australiaWarQuestions = {
                new Question("Australia is officially know as...?", new String[] {"The Commonwealth of Australia", "The republic of Australia", "The United States of Australia", "The Kingdom of Australia"}),
                new Question("Australia comprises mainland and...?", new String[] {"The Island of Tasmania", "The Christmas Island", "Nauru", "Northern Territory"}),
                new Question("Australia is the largest country in?", new String[] {"Oceania", "Asia", "The Atlantic Ocean", "The Pacific Ocean"}),
                new Question("Australia has a population of:", new String[] {"26 million", "32 million", "240 million", "20 million"}),
                new Question("Australia’s population is heavily concentrated in?", new String[] {"The Eastern Seaboard", "Queensland", "Victoria", "Tasmania"}),
                new Question("Indigenous Australia inhabited the continent for?", new String[] {"65,000 years", "70,000 years", "6500 years", "60,000 years"}),
                new Question("Dutch explorers named Australia what?", new String[] {"New Holland", "New Hampshire", "New Haven", "The Commonwealth of Australia"}),
                new Question("On 1st January of which year did Australian colonies federate to form the Commonwealth?", new String[] {"1901", "1922", "1857", "1908"}),
                new Question("What would you describe the political system of Australia as?", new String[] {"Liberal Democratic", "Communistic", "Dictatorship", "Socialistic"}),
                new Question("Political power decreases in which order in Australia?", new String[] {"Federal, State, Territorial", "State, Federal, Territorial", "Provincial, State, Territorial", "Territorial, Federal, Provincial"}),
                new Question("The Federal government is divided into how many branches?", new String[] {"3", "5", "2", "7"}),
                new Question("The senate is a part of?", new String[] {"Legislature", "Congress", "The National Assembly", "Judiciary"}),
                new Question("The House of Representatives is known as?", new String[] {"The lower house", "A territory", "The Senate", "The House of Commons"}),
                new Question("How many seats does ACT get in the senate?", new String[] {"2", "4", "6", "12"}),
                new Question("The Australian Labour party is?", new String[] {"A centre left party", "A liberal party", "A conservative party", "An eco-friendly party"}),
                new Question("The Australian Defence Force has how many full-time personnel?", new String[] {"85,000", "82,000", "80,000", "65,000"}),
                new Question("When was the Australian Defence force formed?", new String[] {"1976", "1978", "1974", "1901"}),
                new Question("Australia’s culture is largely?", new String[] {"Western", "Indigenous", "Asian", "Eastern"}),
                new Question("Which is a home-grown Australian sport?", new String[] {"Australian Rules Football", "Rugby", "Australian Tennis", "Australian Cricket"}),
                new Question("Australia has abundant access to?", new String[] {"Fish", "Chicken", "Sheep", "Cows"})
        };
        Question[] australiaRevoltQuestions = {};
        countryArrayList.add(new Country("Australia",-25.2744,133.7751, 1, 999, 2, australiaWarQuestions, australiaRevoltQuestions));

        Question[] indonesiaWarQuestions = {
                new Question("Indonesia encompasses how many islands?", new String[] {"17000", "12000", "1200", "1700"}),
                new Question("The capital is located on the island called?", new String[] {"Java", "Kiwi", "Bali", "Aceh"}),
                new Question("What percentage of the country’s population is in Java (approximate)?", new String[] {"50%", "70%", "40%", "20%"}),
                new Question("In terms of population, Indonesia is ranked?", new String[] {"4th in the world", "6th in the world", "5th in the world","1st in the world"}),
                new Question("Native Melanesian people have inhabited these islands for how long before further migrations?", new String[] {"45000 years", "65000 years", "100000 years", "25000 years"}),
                new Question("Europeans first arrived in which year?", new String[] {"1512", "1612", "1619", "1510"}),
                new Question("Indonesian independence was proclaimed in which year?", new String[] {"1945", "1947", "1901", "1912"}),
                new Question("The president is elected for a term of how many years?", new String[] {"5", "2", "7", "4"}),
                new Question("The highest level of judicial branch is known as?", new String[] {"The Supreme Court", "The Federal Constitutional Court", "The Supreme Convenor", "The High Court"}),
                new Question("The legislature consists of 2 houses, namely?", new String[] {"The People’s Representative Council, The Regional Representative Council", "The Senate, The Congress", "The National Assembly, The Senate", "The House of Commons, The House of Lords"}),
                new Question("Who elects the cabinet?", new String[] {"The president", "The Prime Minister", "The People’s Representative Council", "The senate"}),
                new Question("Why is a military draft not required in Indonesia?", new String[] {"Because voluntary recruitment provides enough manpower", "Because Indonesia does not have a standing army", "Because Indonesia does not believe in violence", "Because the president does not allow one"}),
                new Question("The highest position in the Indonesian National Armed forces is the?", new String[] {"Panglima", "Pangolian", "Pandora", "Poseidon"}),
                new Question("The Commander of the Indonesian National Armed Forces reports to the?", new String[] {"The President", "The Minister of Defence", "The Governor General", "Prime Minister"})
        };
        Question[] indonesiaRevoltQuestions = {
                new Question("The biggest influence on Indonesia’s modern entertainment is which country?", new String[] {"India", "Nepal", "Malaysia", "Singapore"}),
                new Question("Traditional forms of entertainment include?", new String[] {"The wayang shadow puppets", "The bhangra", "The hulu dance", "The shadow sword dancers"}),
                new Question("Indonesian cuisine has a lot of variety because?", new String[] {"Number of regions Indonesia covers", "They like food", "Different tribes have different tastes", "They have many spices"}),
                new Question("Most Indonesian’s practice which religion?", new String[] {"Islam", "Hinduism", "Buddhism", "Christianity"}),
                new Question("What aspect of Indonesia’s history unites it against a breach of sovereignty?", new String[] {"Colonialism", "Massacre", "Migration", "Ethnic diversity"}),
                new Question("A shared identity has developed with the motto?", new String[] {"Unity in diversity", "Unity, Strength, Discipline", "Unity is freedom", "Unity is strength"}),
                new Question("Indonesian culture has not been affected by which religion?", new String[] {"Zionism", "Buddhism", "Hinduism", "Islam"}),
                new Question("Iconic Indonesian dishes include?", new String[] {"Nasi Goreng", "Indomie", "Ramen", "Kimchi"}),
                new Question("The western world has influenced Indonesia in?", new String[] {"Science", "Sailing", "Dancing", "Football"}),
                new Question("Largest ethnic group in Indonesia is?", new String[] {"Javanese", "Riau", "Lampung", "Balinese"})
        };
        countryArrayList.add(new Country("Indonesia",-0.7893,113.9213,2,3,4, indonesiaWarQuestions, indonesiaRevoltQuestions));

        Question[] koreaWarQuestions = {
                new Question("The president acts as?", new String[] {"The Head of State", "The Prime Minister", "The Federal Minister", "The Governor General"}),
                new Question("Korea’s government has how many branches?", new String[] {"3", "1", "6", "8"}),
                new Question("The executive branch of the government is headed by?", new String[] {"The President", "The Senate", "The House of Commons", "The Secretary of State"}),
                new Question("The head of the legislative branch is?", new String[] {"The National Assembly", "The Senate", "The House of Representatives", "The Executive branch"}),
                new Question("Judges of the constitutional court are appointed by?", new String[] {"The executive and legislative branches", "The president", "The Judicial branch", "The supreme court"}),
                new Question("Seoul, the capital city, is ranked by metropolitan populations?", new String[] {"4th", "8th", "6th", "5th"}),
                new Question("South Korea has the world’s most?", new String[] {"Fastest internet speed", "Spicy food", "Highest temperature", "Drunk population"}),
                new Question("There is evidence of habitation on the Korean peninsula as far back as?", new String[] {"The stone age", "The middle ages", "Renaissance", "Medieval times"}),
                new Question("Korea was occupied by many different countries because?", new String[] {"Of its geographical location", "Japan keeps attacking them", "Soviets kept attacking them", "The Mongols destroyed their defences"}),
                new Question("The Republic of Korea Armed Forces were established after liberation from?", new String[] {"The Japanese empire", "Soviet Russia", "The Mongols", "North Korea"}),
                new Question("Korea’s Armed Forces developed rapidly after which war?", new String[] {"The Korean war", "World War 1", "The Japanese Sino wars", "World War 2"}),
                new Question("Compulsory military service requires males over what age to serve?", new String[] {"16", "12", "15", "18"}),
                new Question("Does Korea manufacture weapons or import from other countries?", new String[] {"Both", "Exports", "Manufactures", "Korea doesn’t have weapons"})
        };
        Question[] koreaRevoltQuestions = {
                new Question("Which pop culture aspect has thrown Korea into the international spotlight?", new String[] {"K-pop", "Korean dance moves", "Korean fashion", "Korean movies"}),
                new Question("Korean music has its roots in?", new String[] {"Folk based ballads", "Rock", "Pop music", "Classical music"}),
                new Question("Which Korean movie won the Academy award for best picture?", new String[] {"Parasite", "Veteran", "The thieves", "Ode to my Father"}),
                new Question("Korean cuisine is mainly based on?", new String[] {"Rice", "Eggs and bread", "Spices", "Pasta"}),
                new Question("Korean food is often accompanied by?", new String[] {"Side dishes", "Kimchi", "Soft drinks", "Desserts"}),
                new Question("Notable Korean dishes include?", new String[] {"Kimchi", "Nasi Goreng", "Indomie", "Ramen"}),
                new Question("The traditional culture of Korea was prevalent in?", new String[] {"Early nomadic tribes", "The north Koreans", "The Chinese invaders", "The Japanese invaders"}),
                new Question("A mixture of the Korean and English langiages is referred to as?", new String[] {"Konglish", "Pansori", "King Kong", "Minglish"}),
                new Question("Pansori is a traditional form musical art that consists of?", new String[] {"A singer and a drummer", "A violin and a harp", "A stick and a stone", "A singer and a pianist"}),
                new Question("Which culture has influenced Korea the most?", new String[] {"Chinese", "Russian", "Japanese", "Indian"}),
        };
        countryArrayList.add(new Country("South Korea",35.9078,127.7669,4,5,6, koreaWarQuestions, koreaRevoltQuestions));

        Question[] chinaWarQuestions = {
                new Question("What is China’s rank by land size in the world?", new String[] {"3rd", "1st", "4th", "5th"}),
                new Question("China’s earliest civilization emerged in the basin of which river?", new String[] {"Yellow rover", "Yangtze river", "Mekong river", "Nile river"}),
                new Question("Who united China under the first Chinese empire?", new String[] {"Qin", "Xia", "Han", "Huang"}),
                new Question("Some great Chinese inventions include?", new String[] {"The compass", "The great wall", "The canon", "The air gun"}),
                new Question("The Chinese civil war resulted in a division of territory between?", new String[] {"People’s Republic of China and Taiwan", "Qin and Genghis Khan", "People’s Republic of China and Japan", "Mainland China and the Mongols"}),
                new Question("Who holds ultimate power in China?", new String[] {"The General Secretary of the Communist party", "The premier", "The Prime Minister", "The Governor General"}),
                new Question("Why does the General Secretary have de-facto control of the government?", new String[] {"They hold the offices of President and Chairman of Central Military Commission as well", "They lead the communist movement", "They are supreme", "They have the bigger guns"}),
                new Question("Who oversees legislation and the operations of the government?", new String[] {"The National People’s Congress", "The State Council", "The Communist party", "The General Secretary of the party"}),
                new Question("Which area does the Supreme People’s Court not have jurisdiction over?", new String[] {"Hong Kong", "Shanghai", "Beijing", "Xinjiang"}),
                new Question("China is recognised as a military superpower because", new String[] {"They have nuclear weapons", "They have a large military", "They have good military technology", "They are good warriors"}),
                new Question("The People’s Liberation army was formed as a response to?", new String[] {"Civil war", "Mongols", "Japan", "North Korea"}),
                new Question("Which is a non-conventional branch of the People’s Liberation Army?", new String[] {"Rocket force", "Aerospace Division", "Battlefront squadron", "Red army"})
        };
        Question[] chinaRevoltQuestions = {
                new Question("Why has Chinese civilization considered the dominant culture of east Asia?", new String[] {"They are one of the earliest ancient civilizations", "They have good medicine", "The Chinese have been conquering and spreading their culture", "It has exerted force upon others"}),
                new Question("The two minor hierarchical classes include?", new String[] {"Merchants and craftsmen", "Peasants and beggars", "Craftsmen and Blacksmith", "Merchant and landlord"}),
                new Question("Han Chinese constitute approximately what percentage of the Chinese population?", new String[] {"92%", "99%", "86%", "45%"}),
                new Question("Which is part of the three teachings that have shaped Chinese culture?", new String[] {"Confucianism, Taoism, Buddhism", "Taoism, Islam, Christianity", "Tradition, Buddhism, Confucianism", "Buddhism, Nirvana, Taoism"}),
                new Question("Traditional Chinese medicine includes?", new String[] {"Herbal medicine", "Massages", "Healing flames", "Cult practices"}),
                new Question("Chinese family life can be?", new String[] {"Rigid and hierarchical", "Pretty chill", "Authoritarian", "Abusive"}),
                new Question("The most important Chinese festival is?", new String[] {"Chinese New Year", "Lunar festival", "Lantern festival", "Mid-Autumn festival"}),
                new Question("Young people in china enjoy playing which game?", new String[] {"Badminton", "Cricket", "Rugby", "Football"}),
                new Question("How many major cuisines are there within china?", new String[] {"8", "7", "6", "5"}),
                new Question("What food forms the staple diet in the south of china?", new String[] {"Rice", "Pork", "Eggs", "Bread"}),
        };
        countryArrayList.add(new Country("China", 35.8617, 104.1954,6,7,8, chinaWarQuestions, chinaRevoltQuestions));

        return countryArrayList;
    }

    public static Country searchCountry(String countryName) {
        for(Country currentCountry : getCountries()) {
            if(currentCountry.getName().equals(countryName)) {
                return currentCountry;
            }
        }

        return null;
    }
}
