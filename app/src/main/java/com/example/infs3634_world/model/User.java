package com.example.infs3634_world.model;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.infs3634_world.network.LeaderboardAPI;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.example.infs3634_world.network.NetworkClient.getRetrofitClient;

public class User implements java.io.Serializable {
    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("chapter")
    @Expose
    private Integer chapter;

    private static final String filename = "CONFIG.TXT";

    public User(String username, String token, Integer chapter) {
        this.username = username;
        this.token = token;
        this.chapter = chapter;
    }

    public String getUsername() {
        return this.username;
    }

    public String getToken() {
        return this.token;
    }

    public Integer getChapter() {
        return this.chapter;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setChapter(Integer chapter) {
        this.chapter = chapter;
    }

    public static User loadUser(Context context) {
        try {
            FileInputStream fis = context.openFileInput(filename);
            ObjectInputStream is = new ObjectInputStream(fis);
            User user = (User) is.readObject();
            is.close();
            fis.close();
            return user;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean saveUser(Context context) {
        try {
            FileOutputStream fos = context.openFileOutput(filename, Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(this);
            os.close();
            fos.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

//    public boolean saveChapter() {
//        final String funcName = "saveChapter";
//
//        boolean saved = false;
//
//        Retrofit retro = getRetrofitClient();
//        LeaderboardAPI leaderboard = retro.create(LeaderboardAPI.class);
//        Call call = leaderboard.updateChapter(this);
//
//        call.enqueue(new Callback() {
//            @Override
//            public void onResponse(Call call, Response response) {
//                /*This is the success callback. Though the response type is JSON, with Retrofit we get the response in the form of WResponse POJO class
//                 */
//                NetworkResponse nResponse = (NetworkResponse) response.body();
//                Gson gson = new GsonBuilder().create();
//                String notificationText = "";
//
//                if(response.isSuccessful()) {
//                    Log.d(funcName, "(" + funcName + ") response was successful!");
//                    if(nResponse.status.equals(200)) {
//                        // Successful user login
//                        Log.d(funcName, "");
//                    } else {
//                        Log.d(funcName, "");
//                    }
//                } else {
//                    Log.d(funcName, "(" + funcName + ") response was not successful!");
//                }
//            }
//            @Override
//            public void onFailure(Call call, Throwable t) {
//                Log.d(funcName, "(" + funcName + ") Error retrieving from remote resource!");
//            }
//        });
//        return false;
//    }

    public static boolean logoutUser(Context context) {
        try {
            context.deleteFile(filename);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
