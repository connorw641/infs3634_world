package com.example.infs3634_world.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NetworkResponse {
    @SerializedName("status")
    @Expose
    public Integer status;

    @SerializedName("response")
    @Expose
    public Object response;
}
