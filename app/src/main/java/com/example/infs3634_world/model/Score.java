package com.example.infs3634_world.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Score {
    @SerializedName("value")
    @Expose
    private Integer value;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("chapter")
    @Expose
    private Integer chapter;

    public int getValue() {
        return value;
    }

    public String getUsername() {
        return username;
    }

    public int getChapter() {
        return chapter;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setChapter(int chapter) {
        this.chapter = chapter;
    }
}
