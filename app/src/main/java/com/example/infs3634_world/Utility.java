package com.example.infs3634_world;

import android.content.Context;
import android.util.Log;

import com.example.infs3634_world.model.User;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;

public class Utility {
    private static final String datFile = "config.txt";
    private static final String TAG = "Utility";

    public static boolean saveChapter() {
        return false;
    }

    public static boolean uploadChapter() {
        return false;
    }

//    public static boolean saveLoginDetails(User user, Context context) {
//        final String funcTAG = TAG + ".saveLgnDtl";
//        String output = "";
//
//        try {
//            FileOutputStream fos = context.openFileOutput(datFile, Context.MODE_PRIVATE);
//            ObjectOutputStream os = new ObjectOutputStream(user);
//            os.writeObject(this);
//            os.close();
//            fos.close();
//
//            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(datFile, Context.MODE_PRIVATE));
//            outputStreamWriter.write(user.getUsername() + "," + user.getToken());
//            outputStreamWriter.close();
//            Log.d(funcTAG, "Saved login details to file " + datFile);
//            return true;
//        }
//        catch (IOException e) {
//            Log.e(funcTAG, "File write failed: " + e.toString());
//        }
//
//        return false;
//    }
//
//    public static boolean loadLoginDetails(Context context) {
//        final String funcTAG = TAG + ".loadLgnDtl";
//        String ret = "";
//
//        try {
//            InputStream inputStream = context.openFileInput(datFile);
//
//            if ( inputStream != null ) {
//                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                String receiveString = "";
//                StringBuilder stringBuilder = new StringBuilder();
//
//                while ( (receiveString = bufferedReader.readLine()) != null ) {
//                    stringBuilder.append("\n").append(receiveString);
//                }
//
//                inputStream.close();
//                ret = stringBuilder.toString();
//                Log.d(funcTAG,ret);
//                return true;
//            }
//        }
//        catch (FileNotFoundException e) {
//            Log.e(funcTAG, "File not found: " + e.toString());
//        } catch (IOException e) {
//            Log.e(funcTAG, "Can not read file: " + e.toString());
//        }
//
//        return false;
//    }
}
