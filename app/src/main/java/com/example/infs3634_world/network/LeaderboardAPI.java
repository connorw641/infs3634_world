package com.example.infs3634_world.network;

import com.example.infs3634_world.model.LoginRequest;
import com.example.infs3634_world.model.NetworkResponse;
import com.example.infs3634_world.model.Score;
import com.example.infs3634_world.model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface LeaderboardAPI {

    @POST("/api/login")
    Call<NetworkResponse> login(@Body LoginRequest body);

    @POST("/api/update")
    Call<NetworkResponse> updateChapter(@Body User user);

    @POST("/api/register")
    Call<NetworkResponse> register(@Body LoginRequest body);

    @POST("/api/leaderboard")
    Call<NetworkResponse> leaderboard(@Body User body);

    @POST("/api/leaderboard/{chapter}")
    Call<NetworkResponse> leaderboard_chapter(@Path("chapter") int chapter, @Body User body);

    @POST("/api/leaderboard/my")
    Call<NetworkResponse> leaderboard_my(@Body User body);

    @POST("/api/leaderboard/my/{chapter}")
    Call<NetworkResponse> leaderboard_my_chapter(@Path("chapter") int chapter, @Body User body);

}